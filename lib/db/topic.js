var uid = require('../uid')

module.exports = function (sequelize, DataTypes) {
  var topic = sequelize.define('topic', {
    title: {
      type: DataTypes.STRING
    },
    audience: {
      type: DataTypes.ENUM('Anonymous Guests', 'Anonymous Members', 'Identified Members')
    },
    description: {
      type: DataTypes.TEXT
    },
    hash: {
      type: DataTypes.STRING,
      unique: true,
      defaultValue: function () {
        return uid.generate()
      }
    },
    status: {
      type: DataTypes.ENUM('draft', 'published')
    }
  })

  topic.associate = function (models) {
    topic.hasMany(models.comment, { as: 'Comments' })
  }

  return topic
}
