'use strict'

var fs = require('fs')
var path = require('path')
var Sequelize = require('sequelize')
var basename = path.basename(module.filename)
var bcrypt = require('bcrypt')

var config = {
  database: 'coffeetalkdb',
  username: 'username',
  password: 'password',
  host: 'localhost',
  dialect: 'sqlite',
  logging: msg => console.log(msg)
  // storage: 'dataset.sqlite'
}

if (process.env.NODE_ENV === 'test') {
  config.storage = 'test/testdb.sqlite'
}

var sequelize = new Sequelize(config.database, config.username, config.password, config)

var db = {}

fs.readdirSync(__dirname)
  .filter(function (file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
  })
  .forEach(function (file) {
    var model = require(path.join(__dirname, file))(sequelize, Sequelize)
    db[model.name] = model
  })

Object.keys(db).forEach(function (modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

async function go () {
  const syncOptions = {}
  if (process.env.DROP_ON_LOAD) {
    syncOptions.force = true
  }
  await sequelize.sync(syncOptions)

  const defaultAdminUser = process.env.DEFAULT_ADMIN_USER || 'demo'
  const hashedpw = await bcrypt.hash(process.env.DEFAULT_ADMIN_PASS || 'user', 10)

  await db.user.findOrCreate({
    where: {
      username: defaultAdminUser
    },
    defaults: {
      firstName: 'Demo',
      lastName: 'User',
      username: defaultAdminUser,
      password: hashedpw
    }
  })
}

go()

module.exports = db
