const db = require('./db')
const Op = db.Sequelize.Op

async function addComment (hash, user, anonHash, comment) {
  var topic = await db.topic.findOne({ where: { hash: hash } })
  var sanitizedComment = escapeHtml(comment)

  var newComment = { comment: sanitizedComment, topicId: topic.id }

  if (topic.audience === 'Anonymous Guests') {
    newComment.anonHash = anonHash
  } else if (topic.audience === 'Anonymous Members') {
    if (!user) throw new Error('must be logged in to comment')
    newComment.anonHash = anonHash
  } else if (topic.audience === 'Identified Members') {
    if (!user) throw new Error('must be logged in to comment')
    newComment.userId = user.id
  }

  await db.comment.create(newComment)
}

async function vote (hash, user, anonHash, upordown) {
  var comment = await db.comment.findOne({ where: { hash: hash }, include: [db.topic] })
  var topic = comment.topic

  if (!comment) return { error: 'no comment' }

  var existingVote = await db.vote.findOne({
    where: {
      commentId: comment.id,
      [Op.or]: [{ userId: user ? user.id : undefined }, { anonHash: anonHash }]
    }
  })

  if (existingVote && existingVote.thumbs !== upordown) {
    await existingVote.update({ thumbs: upordown })
    await refreshCommentVotes(comment)
  } else if (existingVote && existingVote.thumbs === upordown) {
    await existingVote.destroy()
    await refreshCommentVotes(comment)
  } else if (!existingVote) {
    var newVote = { thumbs: upordown, commentId: comment.id }
    if (topic.audience === 'Anonymous Guests') {
      newVote.anonHash = anonHash
    } else if (topic.audience === 'Anonymous Members') {
      if (!user) return { error: 'must be logged in to vote' }
      newVote.anonHash = anonHash
    } else if (topic.audience === 'Identified Members') {
      if (!user) return { error: 'must be logged in to vote' }
      newVote.userId = user.id
    }

    await db.vote.create(newVote)
    await refreshCommentVotes(comment)
  }

  return {
    thumbsup: comment.thumbsup,
    thumbsdown: comment.thumbsdown
  }
}

async function refreshCommentVotes (comment) {
  console.log('refreshing votes!')
  // refresh vote counts on comment
  var thumbsup = await db.vote.count({ where: { commentId: comment.id, thumbs: 'up' } })
  var thumbsdown = await db.vote.count({ where: { commentId: comment.id, thumbs: 'down' } })

  return comment.update({
    thumbsup: thumbsup,
    thumbsdown: thumbsdown
  }, {
    where: {
      commentId: comment.id
    }
  })
}

async function fetchComments (topicId) {
  var newest = await db.comment.findAll({ where: { topicId: topicId }, order: [['createdAt', 'DESC']], raw: true })
  var mostPopular = await db.comment.findAll({ where: { topicId: topicId }, order: [['thumbsup', 'DESC']], raw: true })

  return {
    newest,
    mostPopular
  }
}

module.exports = {
  addComment,
  fetchComments,
  vote
}

function escapeHtml (text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  }

  return text.replace(/[&<>"']/g, function (m) { return map[m] })
}
