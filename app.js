require('dotenv').config()

var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var morgan = require('morgan')
var session = require('express-session')
var SequelizeStore = require('connect-session-sequelize')(session.Store)
var passport = require('passport')

var db = require('./lib/db')

var passportSetup = require('./lib/passport-setup')
passportSetup()

var index = require('./routes/index')
var login = require('./routes/login')
var logout = require('./routes/logout')
var craft = require('./routes/craft')
var topic = require('./routes/topic')
var vote = require('./routes/vote')
var locals = require('./routes/locals')
var app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')

app.use(morgan('combined'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use(require('body-parser').urlencoded({ extended: true }))
app.use(session({
  secret: 'verklempt',
  resave: false,
  proxy: true,
  saveUninitialized: false,
  store: new SequelizeStore({ db: db.sequelize })
}))

// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize())
app.use(passport.session())

app.use(locals())
app.use('/', index)
app.use('/craft', craft)
app.use('/login', login)
app.use('/logout', logout)
app.use('/topic', topic)
app.use('/vote', vote)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
