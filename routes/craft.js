var express = require('express')
var router = express.Router()

var topicService = require('../lib/topic.service')

/* GET home page. */
router.get(['/:hash', '/'], async function (req, res, next) {
  if (!req.isAuthenticated()) return res.redirect('/login')

  if (req.params.hash) {
    var existingTopic = await topicService.fetch(req.params.hash)
    if (existingTopic.userId !== req.user.id) {
      return res.redirect('/')
    }

    res.render('craft', {
      title: existingTopic.title,
      description: existingTopic.description,
      audiences: topicService.buildAudiences(existingTopic.audience)
    })
  } else {
    res.render('craft', {
      description: defaultDescription,
      audiences: topicService.buildAudiences()
    })
  }
})

router.post(['/:hash', '/'], async function (req, res) {
  if (!req.isAuthenticated()) return res.redirect('/login')

  var hash
  if (req.params.hash) {
    hash = req.params.hash
    var existingTopic = await topicService.fetch(hash)
    if (existingTopic.userId !== req.user.id) {
      return res.redirect('/')
    }

    await topicService.update(existingTopic, req.body)
  } else {
    try {
      var newTopic = await topicService.create(req.user, req.body)
      hash = newTopic.hash
    } catch (err) {
      return res.render('error', { error: err, message: 'something went wrong' })
    }
  }

  res.redirect(`/topic/${hash}`)
})

router.post('/:hash/status/:status', async function (req, res) {
  if (!req.isAuthenticated()) return res.redirect('/login')

  var hash = req.params.hash
  var status = req.params.status

  var existingTopic = await topicService.fetch(hash)
  if (existingTopic.userId !== req.user.id) {
    return res.redirect('/')
  }

  await topicService.updateStatus(hash, status)

  res.redirect(`/topic/${hash}`)
})

module.exports = router

var defaultDescription = `
## Write your open ended question
Use this space to clearly explain what kind of feedback you're looking for.

 * maybe
 * add some
 * bullets
 * Perhaps link to some [website](http://www.donothingfor2minutes.com/)?

`
