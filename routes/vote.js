var express = require('express')
var router = express.Router()

var commentService = require('../lib/comment.service')

router.post('/:upordown/:hash', async function (req, res, next) {
  var upordown = req.params.upordown

  if (upordown !== 'up' && upordown !== 'down') return res.status(400).json({ error: 'bad vote' })

  var resp = await commentService.vote(req.params.hash, req.user, res.locals.anonHash, upordown)

  return res.json(resp)
})

module.exports = router
