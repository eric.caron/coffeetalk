var express = require('express')
var router = express.Router()
const marky = require('markdown-it')()

var topicService = require('../lib/topic.service')
var commentService = require('../lib/comment.service')

router.get('/', async function (req, res, next) {
  var topics = await topicService.fetchOpenTopics()
  res.locals.title = 'Open Topics'

  res.render('opentopics', { topics: topics, active: { openTopicsNav: true } })
})

router.get('/:hash', async function (req, res, next) {
  var topic = await topicService.fetch(req.params.hash)
  var currentUserId = req.user ? req.user.id : null

  if (!topic) res.redirect('/')

  if (topic.status !== 'published' && (currentUserId !== topic.userId)) {
    res.redirect('/')
  }

  // TODO: cache html in db?
  res.locals.title = `Topic "${topic.title}"`

  res.render('topic', {
    unpublished: topic.status !== 'published',
    ownerIsCurrentUser: currentUserId === topic.userId,
    topic: topic.dataValues,
    htmlDescription: marky.render(topic.description),
    comments: await commentService.fetchComments(topic.id)
  }
  )
})

router.post('/:hash/comment', async function (req, res, next) {
  await commentService.addComment(req.params.hash, req.user, res.locals.anonHash, req.body.comment)

  res.redirect(`/topic/${req.params.hash}`)
})

module.exports = router
