var express = require('express')
var router = express.Router()
var passport = require('passport')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.locals.title = 'Login'
  res.render('login', { active: { loginNav: true } })
})

router.post('/',
  passport.authenticate('local', { failureRedirect: '/login' }),
  function (req, res) {
    res.redirect('/')
  })

module.exports = router
